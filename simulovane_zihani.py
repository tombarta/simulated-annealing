import numpy as np
from scipy.stats import linregress
from problem_classes import ObchodniCestujici, Molekuly
from copy import copy, deepcopy

import logging
import sys

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

def equilibrate(problem, T, n_iter, kb=10):
    # pri kazdem ekvilibrovani po n_iter krocich kontroluji,
    # zda je system jiz v rovnovaze, tedy zda se energie / vzdalenost
    # pohybuje kolem jedne hodnoty. kontrola probiha provedenim linearni regrese
    # a naslednou kontrolou, zde je pravdepodobnost, ze primka ma nulovy sklon > 50%
    p = 0

    while p < 0.5:
        rej_num = 0
        worse_num = 0
        tot_diff = 0
        costs = []
        for i in range(n_iter):
            diff = problem.generate_elementary_move(temperature = T)
            if diff < 0:
                problem.accept_new_configuration()
                tot_diff += diff
            else:
                worse_num += 1
                r = np.random.rand()
                if r < np.exp(-diff / (kb * T)):
                    problem.accept_new_configuration()
                    tot_diff += diff
                else:
                    problem.reject_new_configuration()
                    rej_num += 1
            costs.append(tot_diff)

        p = linregress(range(n_iter // 2), costs[-n_iter // 2:])[3]

        if worse_num > 20:
            rej_perc = rej_num / worse_num
        else:
            rej_perc = np.nan
        logger.debug('{:.1f}% of worse rejected'.format(100 * rej_perc))

    return kb

def simulated_annealing(problem, T0=100, speed=1, schedule='log', T_min=None, kb=10):
    # ke konci vypoctu muze dojit ze dvou duvodu:
    # 1. teplota je nizsi, nez stanovena minimalni (T_min)
    # 2. po poslednich 5 ekvilibracich nedoslo ke zmene energie / delky a ta je na soucasnem znamem minimu
    no_change_streak = 0
    no_better_streak = 0
    tot_min = problem.cost
    best_configuration = None
    k = 2

    if schedule == 'log':
        T_func = lambda x: T0 / np.log(x)
    elif schedule == 'linear':
        T_func = lambda x: T0 / x
    else:
        raise ValueError('schedule needs to be either "log" or "linear"')

    # while cyklus kontroluje, zda se cost nepresala menit
    while no_change_streak < 5:
        T = T_func(k)
        if T_min is not None and T < T_min:
            logger.info('minimal temperature reached. computation finished')
            break
        last_val = problem.cost
        equilibrate(problem, T, 100, kb=kb)
        if np.abs(problem.cost - last_val) / last_val < 1e-5 and problem.cost <= tot_min * (1 + 1e-5) and T < 30:
            no_change_streak += 1
        else:
            no_change_streak = 0

        if problem.cost < tot_min:
            tot_min = problem.cost
            best_configuration = copy(problem.configuration)
            no_better_streak = 0
        else:
            no_better_streak += 1

            if no_better_streak > 200 and best_configuration is not None:
                # pokud po velmi dlouhou dobu nedojde k poklesu energie pod znamou minimalni hodnotu,
                # dojde k vyresetovani stavu na nejlepsi znamou konfiguraci
                problem.set_configuration(best_configuration)
                no_better_streak = 0
                logger.info('resetting')
        logger.info('temperature: {}\t cost: {}\t tot_min: {}'.format(T, problem.cost, tot_min))
        k += speed
    logger.info('done')

if __name__ == '__main__':
    coor = np.random.randn(5,2) * 5
    oc = ObchodniCestujici(coor)
    # mol = Molekuly(coor)
    simulated_annealing(oc)