from problem_classes import ObchodniCestujici
import numpy as np

def read_tsp(filename):
    coordinates = []

    with open(filename, 'r') as f:
        coord_section = False
        for line in f:
            if coord_section == True:
                if not 'EOF' in line:
                    coordinates.append(line[:-1].split(' ')[1:])
                else:
                    break
            elif 'NODE_COORD_SECTION' in line:
                coord_section = True
            else:
                continue
    coordinates = np.array(coordinates).astype(float)

    # import pdb
    # pdb.set_trace()

    oc = ObchodniCestujici(coordinates)
    return oc

if __name__ == '__main__':
    oc = read_tsp('data/quatar/qa194.tsp')