# V tomto souboru jsou definovane jednolive problemy, tedy obchodni cestujici a klastr molekul
# Kazdy problem musi obsahovat:
# * configuration - tedy konfigurace, ve ktere se zrovna nachazi
# * cost - hodnota, kterou se snazime minimalizovat
# * fce generate_elementary_move:
#     vygeneruje elementarni posunuti / zamenu a vrati zmenu cost
# * fce accept_new_configuration / reject_new_configuration:
#     prijme / zamitne konfiguraci vygenerovanou fci generate_elementary_move
# * set_configuration:
#     nastavi konfiguraci - aby bylo mozne snadno restartovat z urciteho bodu

from scipy.spatial.distance import pdist, squareform
from copy import copy
import numpy as np
import matplotlib.pyplot as plt
from itertools import permutations

import logging
logger = logging.getLogger('logger')

class Molekuly:
    def __init__(self, initial_configuration, eps=1, rm=1):
        distances = squareform(pdist(initial_configuration))
        self.configuration = copy(initial_configuration)
        self.n_molecules = len(initial_configuration)
        self.potential = lambda dist: eps * ((rm / dist) ** 12 - 2 * (rm / dist) ** 6)
        self.energy_matrix = self.potential(distances)

        for i in range(self.n_molecules):
            self.energy_matrix[i,i] = 0

        self.cost = self.energy_matrix.sum() / 2
        self.tryout_coordinates = copy(self.configuration)
        self.eps = eps
        self.rm = rm


    def generate_elementary_move(self, temperature, fast_mode=False):
        # elementarni posunuti probiha v nasledujicich krocich:
        # 1. je vybrana nahodna molekula, kterou se bude hybat
        # 2. vygeneruje se posunuti za dane teploty
        # 3. spocte se, jakou energii molekula prispivala pred posunutim a po posunuti

        # 1. VYBER NAHODNE MOLEKULY
        self.random_molecule_index = np.random.randint(self.n_molecules)

        # 2. POSUNUTI MOLEKULY
        if fast_mode == False:
            shift = np.random.randn(2) * (2 * temperature) / 500
        else:
            raise RuntimeError('Fast simulated annealing is not yet implemented')
        self.tryout_coordinates[self.random_molecule_index] += shift

        # 2.1 SPOCTENI PUVODNI ENERGIE MOLEKULY
        old_cost = self.energy_matrix[:,self.random_molecule_index].sum()

        # 2.2 SPOCTENI ENERGIE MOLEKULY PO POSUNUTI
        new_distances = np.linalg.norm(self.configuration - self.tryout_coordinates[self.random_molecule_index], axis=1)
        self.tryout_energies = self.potential(new_distances)
        self.tryout_energies[self.random_molecule_index] = 0
        new_cost = self.tryout_energies.sum()

        self.tryout_cost = self.cost - old_cost + new_cost
        return new_cost - old_cost

    def accept_new_configuration(self):
        self.configuration[self.random_molecule_index] = self.tryout_coordinates[self.random_molecule_index]
        self.energy_matrix[self.random_molecule_index,:] = self.tryout_energies
        self.energy_matrix[:,self.random_molecule_index] = self.tryout_energies
        self.cost = self.tryout_cost

    def reject_new_configuration(self):
        self.tryout_coordinates[self.random_molecule_index] = self.configuration[self.random_molecule_index]
        self.tryout_cost = self.cost

    def set_configuration(self, configuration):
        self.__init__(copy(configuration), self.eps, self.rm)

    def __fast_annealing_rand(self, T):
        pass

    def plot(self, ax=None, **kwargs):
        x = self.configuration[:,0]
        y = self.configuration[:,1]

        if ax is not None:
            ax.scatter(x, y, marker='o', **kwargs)
        else:
            plt.scatter(x, y, marker='o', **kwargs)
            plt.axes().set_aspect('equal')


class ObchodniCestujici:
    def __init__(self, coordinates, initial_configuration=None):
        self.coordinates = coordinates
        self.dist_matrix = self.__compute_dists()
        if initial_configuration is not None:
            self.configuration = initial_configuration
        else:
            self.configuration = np.arange(coordinates.shape[0])
        self.cost = self.__initial_cost_function(self.configuration)
        self.tryout_cost = None
        self.tryout_path = copy(self.configuration)

    def generate_elementary_move(self, temperature):
        # narozdil od posouvani molekul zde neni tak trivialni generovat vetsi zameny pro vetsi teploty
        # misto toho pri vysokych teplotach je provedeno vice jednoduchych zamen najednou
        #
        # jednoducha zamena znamena nasledujici:
        # 1. jsou vybrany 2 mesta
        # 2. pro kazde z mest je spoctena vzdalenost od predchazejiciho mesta a k nasledujicimu mestu
        # 3. tyto dve mesta si vymeni pozice
        # 4. s temito novymi pozicemi je opakovan bod 2.
        # 5. rozdil mezi souctem puvodnich a novych vzdalenosti je celkovy rozdil v delce trasy
        #
        # generovani zamen tedy probiha v nasledujich krocich:
        # A. vypocteni, ke kolika zamenam dojde
        # B. tolikrat je vykonana elementarni zamena

        # A. vypocet poctu zamen
        n_swaps = int(min(temperature // 30 + 1, len(self.configuration) - 1))

        self.swap_ix = []
        old_cost = 0
        new_cost = 0

        # B. n_swap-krat dojde k elementarni zamene dvou mest
        for i in range(n_swaps):
            # 1. nahodny vyber 2 mest
            self.swap_ix.append(np.random.choice(range(len(self.configuration)), 2, replace=False))

            # 2.1 spocteni vzdalenosti ve stavu pred zamenou
            #   zde je dulezite, ze se vypocet provadi jiz na "pokusne" trase
            #   kdyz dochazi k vice zamenam, tak je dulezite vzdalenosti pocitat PRED aktualni zamenou,
            #   ale PO vsech predchozich
            old_cost += sum(self.__path_cut_cost(ix, self.tryout_path) for ix in self.swap_ix[-1])

            # provedeni elementarni zameny dvojice
            self.tryout_path[self.swap_ix[-1][0]], self.tryout_path[self.swap_ix[-1][1]] = \
                self.tryout_path[self.swap_ix[-1][1]], self.tryout_path[self.swap_ix[-1][0]]

            # 2.2 vypocet novych vzdalenosti
            new_cost += sum(self.__path_cut_cost(ix, self.tryout_path) for ix in self.swap_ix[-1])

        self.tryout_cost = self.cost - old_cost + new_cost
        return new_cost - old_cost

    def __path_cut_cost(self, swap_index, path):
        if swap_index == 0:
            path_cut = [path[-1], path[0], path[1]]
        elif swap_index == len(path) - 1:
            path_cut = [path[-2], path[-1], path[0]]
        else:
            path_cut = [path[swap_index - 1], path[swap_index], path[swap_index + 1]]

        return self.dist_matrix[path_cut[0], path_cut[1]] + \
               self.dist_matrix[path_cut[1], path_cut[2]]

    def accept_new_configuration(self):
        for exchange in self.swap_ix:
            self.configuration[exchange] = self.tryout_path[exchange]
        self.cost = self.tryout_cost

    def reject_new_configuration(self):
        for exchange in self.swap_ix:
            self.tryout_path[exchange] = self.configuration[exchange]
        self.tryout_cost = self.cost

    def set_configuration(self, configuration):
        self.__init__(coordinates=self.coordinates, initial_configuration=configuration)

    def __compute_dists(self):
        return squareform(pdist(self.coordinates))

    def __initial_cost_function(self, path):
        cost = 0
        for edge in zip(path, np.roll(path, shift=-1)):
            cost += self.dist_matrix[edge[0], edge[1]]
        return cost

    def brute_force_check(self):
        best_path = copy(self.configuration)
        best_score = self.cost
        unchanged = True

        for perm in permutations(range(1, len(self.configuration))):
            path = [0] + list(perm)
            score = self.__initial_cost_function(path)
            if score * (1 + 1e-5) < best_score:
                best_score = score
                best_path = path
                unchanged = False
        return {
            'unchanged': unchanged,
            'best score': best_score,
            'best path': best_path
        }

    def plot(self, ax=None, **kwargs):
        coor_sorted = np.array([self.coordinates[i] for i in self.configuration])
        x = coor_sorted[:,0]
        y = coor_sorted[:,1]
        x = np.append(x, x[0])
        y = np.append(y, y[0])

        if ax is not None:
            ax.plot(x, y, marker='o', **kwargs)
        else:
            plt.plot(x, y, marker='o', **kwargs)
            plt.axes().set_aspect('equal')