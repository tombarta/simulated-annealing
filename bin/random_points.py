import numpy as np
import sys
import matplotlib.pyplot as plt
from problem_classes import ObchodniCestujici
from simulovane_zihani import simulated_annealing


if __name__ == '__main__':
    n_cities = int(sys.argv[1])
    coor = np.random.randn(n_cities, 2)
    oc = ObchodniCestujici(coor)
    simulated_annealing(oc, T0=n_cities * 5, schedule='linear', speed=5)

    if n_cities <= 10:
        print('checking correctness with brute force')
        if oc.brute_force_check()['unchanged']:
            print('SA found global minimum')
        else:
            print('minimum found by SA is only local. global minimum not found by SA')

    oc.plot()
    plt.show()
    plt.savefig('img/random_points.png')