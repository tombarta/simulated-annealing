from parsing import read_tsp
from simulovane_zihani import simulated_annealing
import matplotlib.pyplot as plt


if __name__ == '__main__':
    oc = read_tsp('data/djibouti/dj38.tsp')
    simulated_annealing(oc, T0=500, schedule = 'linear', speed=5e-1)
    oc.plot()
    plt.show()
    plt.savefig('img/djibouti.png')