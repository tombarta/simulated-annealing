import numpy as np
import sys
import matplotlib.pyplot as plt
from problem_classes import Molekuly
from simulovane_zihani import simulated_annealing


if __name__ == '__main__':
    n_molecules = int(sys.argv[1])
    coor = np.random.randn(n_molecules, 2)
    mol = Molekuly(coor)
    mol.plot(label='puvodni')

    simulated_annealing(mol, T0=25, schedule='log', speed=2, T_min=3e0, kb=5e-3)

    mol.plot(label='po optimalizaci')
    plt.legend()
    plt.show()
    plt.savefig('img/vdw.png')